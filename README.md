# BTVN
1. API: 

    $api->get('/referral-comment/{id}', 'Api\ReferralCommentController@getDetailReferralComment');

    $api->get('/detail-contact/{id}', 'Api\ContactController@detailContact');

    ….

File:

    backend/app/Http/Controllers/Api/ReferralCommentController.php

Function:

    ReferralCommentController@ getDetailReferralComment

![Screenshot](Screenshot_from_2021-03-08_13-41-31.png)

Cách tối ưu: Có thể dùng “Route model binding trong laravel" 

    Link tham khảo: https://viblo.asia/p/route-model-binding-trong-laravel-PwRGgVwnvEd 

    Hoặc tham khảo

    File:

        backend/app/Controller/CompanyController.php

    Function:

        CompanyController@purchaseContract

![Screenshot](Screenshot_from_2021-03-08_13-51-53.png)

2. Phù phép whereLike trong dự án

File:

    backend/app/Entities/BriefingSession.php

Function:

    BriefingSession@list

![Screenshot](Screenshot_from_2021-03-08_13-20-41.png)


Cách tối ưu: Có thể tham khảo

    File:

        backend/app/Http/Controller/Api/Admin/AdminController.php

    Function:

        AdminController@showListUser

![Screenshot](Screenshot_from_2021-03-08_13-17-14.png)

3. Loại bỏ những use không dùng đến

File:

    backend/app/Http/Controller/Api/ConversationController.php

Use thừa:

    use Tymon\JWTAuth\Facades\JWTAuth;

    use App\Http\Controllers\Api\NotificationController;

4. Loại bỏ những hàm thừa không dùng đến

Ví dụ: Function: MessageController

![Screenshot](Screenshot_from_2021-03-08_13-54-32.png)

5. Don't repeat yourself (DRY) - Đừng tự lặp lại chính mình

File: backend/app/Http/Controllers/Api/PostController.php

Function: index và getPostFollowsUser

![Screenshot](Screenshot_from_2021-03-08_13-55-39.png)
![Screenshot](Screenshot_from_2021-03-08_13-56-00.png)

    Phần request tìm kiếm có thế viết lại một chỗ và tái sử dụng lại được đoạn code đó bằng cách gọi vào dùng nếu cần, đỡ phải viết nhiều nơi

6. Mass assignment - Gán giá trị hàng loạt

File: backend/app/Http/Controllers/Api/CompanyController.php

Function: updateProfile

![Screenshot](Screenshot_from_2021-03-08_17-30-54.png)

Cách khắc phục có thể tham khảo:

![Screenshot](Screenshot_from_2021-03-08_17-33-41.png)

    Hoặc: 

Screenshot_from_2021-03-08_17-45-21.png)


7. Tuân theo quy ước đặt tên của Laravel

File: backend/database/migrations/2020_04_27_185844_likeables.php

8. Sử dụng các files config và language, hằng số thay vì văn bản trong code

File: backend/app/Http/Controllers/Api/Asets/UploadFileController.php

Function: callFileUrl

9. Không lấy dữ liệu trực tiếp từ tệp .env

File: backend/app/Http/Controllers/Api/AuthController.php

Function: confirmRegistration

![Screenshot](Screenshot_from_2021-03-08_17-19-37.png)

    Cách khắc phục:

        Nên đưa vào file config, sau đó gọi từ file config vào để dùng

        Tham khảo File: backend/app/Http/Controllers/Api/InquiryController.php

        Function: sendInquiryCompany

![Screenshot]Screenshot_from_2021-03-08_17-22-31.png)

10. Sử dụng Transaction trong MySQL

File: backend/app/Http/Controllers/Api/Users/UsersController.php

Function: postDelete

![Screenshot](Screenshot_from_2021-03-08_17-24-24.png)


11. Ưu tiên dùng Eloquent hơn Query Builder, raw SQL

File: backend/app/Http/Controllers/Api/ReferralCommentController.php
Function: getReferralComment

![Screenshot](Screenshot_from_2021-03-08_17-27-14.png)
